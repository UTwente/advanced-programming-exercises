#!/usr/bin/env bash
set -euf -o pipefail

files="../1/notes.md ../2/notes.md"
zipfiles="../1 ../2"
tex="content.tex"
rep="report"
zip="hofstra-Ex2.zip"
out="$1"

_pandoc() {
  for md in $files; do
    pandoc "${md}" \
      --from=markdown_github+tex_math_single_backslash+tex_math_dollars-hard_line_breaks+link_attributes+raw_tex \
      --to="$1" \
      --listings \
      --mathml \
      --tab-stop 4 \
      --smart
  done
}

_mkzip() {
  [[ -e "${zip}" ]] && rm "${zip}"
  zrep=${zip/zip/pdf}
  cp -a "${rep}.pdf" "${zrep}"

  7z a "${zip}" \
    "${zrep}" \
    ${zipfiles}
}

case "${out}" in
  "html")
    _pandoc "html"
    ;;
  "latex")
    _pandoc "latex" > "${tex}"
    latexmk -xelatex "${rep}.tex"
    ;;
  "zip")
    _pandoc "latex" > "${tex}"
    latexmk -xelatex "${rep}.tex"
    _mkzip
    ;;
esac
