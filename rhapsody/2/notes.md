# Dishwasher Labs
## Lab 4
The tutorial was done without any strange behaviour.
All names were replaced by Dutch equivalents to satisfy the renaming requirement.
The resulting state chart:

![state chart actief](actiefdiagram.jpg){ width=70% }

The resulting state chart when active:

![state chart actief actief](actiefdiagramactief.jpg){ width=70% }

And of course the flow chart for `setup()`:

![diagram setup](setupdiagram.jpg){ width=50% }

The OMD is:

![omd](omd4.jpg){ width=40% }

## Lab 5
The 'Add Breakpoint' menu does not do anything unless the object has been instantiated.
This is not explained in the tutorial and hard to debug because no error message is given: the menu just does nothing.
Otherwise the breakpoint behaves as described.
Output:

```
<State Entered Afwasmachinetje[0] ROOT.actief.draaiend.open> Break point Active
*********************************************
 Executable reached breakpoint
```

Missing from the instructions is that the model has to be paused to follow the instructions.
The process happens instantaneously.

The panel setup works well, with one sleight bug: text is formatted differently depending on it being selected or not.
Resulting panel:

![panel](paneel.jpg)

## Lab 6
The main OMD now looks like:

![omd project](omd6.1.jpg){width=80%}

The state chart looks like:

![state with extra](state6.6.jpg)

The OMD of the builder:

![omd builder](omd6.6bouw.jpg)

The sequence diagram when run with the instructed key looks like:

![seq diag](seqdiag6.jpg)

## Lab 7
Whether the webpage actually loads seems to be determined by fair dice roll,
but if the page loads it works:

![webview](webview7.1.PNG)

The new OMD for the builder looks like:

![builder](omd7.4bouw.jpg){width=80%}

The sequence diagram works and the output of the program is:

```
AC-motor aan
AC-motor uit
```
