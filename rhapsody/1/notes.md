# Hello World & Count Down

## Hello World
The OMD drawing is:

![omd1](omd1.jpg){width=30%}

The output is:

```
Constructed
```

## Count Down
Instructions were followed. OMD:

![omd2](omd2.jpg){width=25%}

Statechart:

![statechart1](statechart1.jpg){width=40%}

```
Constructed
Started
Count = 10
Count = 9
Count = 8
Count = 7
Count = 6
Count = 5
Count = 4
Count = 3
Count = 2
Count = 1
Count = 0
Done
Destroyed
```
