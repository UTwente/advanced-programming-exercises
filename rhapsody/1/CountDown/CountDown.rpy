I-Logix-RPY-Archive version 8.10.0 C++ 8169320
{ IProject 
	- _id = GUID c3d0a14c-a572-4732-95d4-91cf16fa7505;
	- _myState = 8192;
	- _properties = { IPropertyContainer 
		- Subjects = { IRPYRawContainer 
			- size = 1;
			- value = 
			{ IPropertySubject 
				- _Name = "General";
				- Metaclasses = { IRPYRawContainer 
					- size = 1;
					- value = 
					{ IPropertyMetaclass 
						- _Name = "Model";
						- Properties = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IProperty 
								- _Name = "ModelCodeAssociativityFineTune";
								- _Value = "Bidirectional";
								- _Type = Enum;
								- _ExtraTypeInfo = "Bidirectional,RoundTrip,CodeGeneration,Never";
							}
						}
					}
				}
			}
		}
	}
	- _name = "CountDown";
	- _modifiedTimeWeak = 11.22.2016::12:21:46;
	- _lastID = 1;
	- _UserColors = { IRPYRawContainer 
		- size = 16;
		- value = 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 
	}
	- _defaultSubsystem = { ISubsystemHandle 
		- _m2Class = "ISubsystem";
		- _filename = "Default.sbs";
		- _subsystem = "";
		- _class = "";
		- _name = "Default";
		- _id = GUID f6003de6-2a28-4505-9967-fdb663ac3dcb;
	}
	- _component = { IHandle 
		- _m2Class = "IComponent";
		- _filename = "Test.cmp";
		- _subsystem = "";
		- _class = "";
		- _name = "Test";
		- _id = GUID d4aad5ad-d232-4758-8942-f0e81b6cf7be;
	}
	- Multiplicities = { IRPYRawContainer 
		- size = 4;
		- value = 
		{ IMultiplicityItem 
			- _name = "1";
			- _count = -1;
		}
		{ IMultiplicityItem 
			- _name = "*";
			- _count = -1;
		}
		{ IMultiplicityItem 
			- _name = "0,1";
			- _count = -1;
		}
		{ IMultiplicityItem 
			- _name = "1..*";
			- _count = -1;
		}
	}
	- Subsystems = { IRPYRawContainer 
		- size = 1;
		- value = 
		{ ISubsystem 
			- fileName = "Default";
			- _id = GUID f6003de6-2a28-4505-9967-fdb663ac3dcb;
		}
	}
	- Diagrams = { IRPYRawContainer 
		- size = 1;
		- value = 
		{ IDiagram 
			- _id = GUID e5474ab2-2af4-4d6c-aa65-a1737cff4305;
			- _myState = 8192;
			- _properties = { IPropertyContainer 
				- Subjects = { IRPYRawContainer 
					- size = 1;
					- value = 
					{ IPropertySubject 
						- _Name = "Format";
						- Metaclasses = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertyMetaclass 
								- _Name = "Class";
								- Properties = { IRPYRawContainer 
									- size = 8;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,34,84,148";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Weight@Child.NameCompartment@Name";
										- _Value = "700";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "109,163,217";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineStyle";
										- _Value = "0";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
						}
					}
				}
			}
			- _name = "Overview";
			- _modifiedTimeWeak = 1.2.1990::0:0:0;
			- _lastModifiedTime = "11.22.2016::12:14:56";
			- _graphicChart = { CGIClassChart 
				- _id = GUID 10e5cbb0-d743-44ad-a053-9c1d11779526;
				- m_type = 0;
				- m_pModelObject = { IHandle 
					- _m2Class = "IDiagram";
					- _id = GUID e5474ab2-2af4-4d6c-aa65-a1737cff4305;
				}
				- m_pParent = ;
				- m_name = { CGIText 
					- m_str = "";
					- m_style = "Arial" 10 0 0 0 1 ;
					- m_color = { IColor 
						- m_fgColor = 0;
						- m_bgColor = 0;
						- m_bgFlag = 0;
					}
					- m_position = 1 0 0  ;
					- m_nIdent = 0;
					- m_bImplicitSetRectPoints = 0;
					- m_nOrientationCtrlPt = 8;
				}
				- m_drawBehavior = 0;
				- m_bIsPreferencesInitialized = 0;
				- elementList = 2;
				{ CGIClass 
					- _id = GUID 33e3865d-cfb9-4022-bedd-252c952babd4;
					- m_type = 78;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClass";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "";
						- _name = "TopLevel";
						- _id = GUID 5d908229-24ed-470c-a973-5c12d43259ca;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "TopLevel";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 5;
					}
					- m_drawBehavior = 0;
					- m_bIsPreferencesInitialized = 0;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 0 ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 2;
						- value = 
						{ CGICompartment 
							- _id = GUID 23a3b63a-1da3-47cc-9810-71acdc989751;
							- m_name = "Attribute";
							- m_displayOption = Explicit;
							- m_bShowInherited = 0;
							- m_bOrdered = 0;
							- Items = { IRPYRawContainer 
								- size = 0;
							}
						}
						{ CGICompartment 
							- _id = GUID f09fa5c7-df32-415b-823e-7063168137b6;
							- m_name = "Operation";
							- m_displayOption = Explicit;
							- m_bShowInherited = 0;
							- m_bOrdered = 0;
							- Items = { IRPYRawContainer 
								- size = 0;
							}
						}
					}
					- Attrs = { IRPYRawContainer 
						- size = 0;
					}
					- Operations = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIClass 
					- _id = GUID 909492f7-db6b-4004-8bd9-9e002932f07f;
					- _properties = { IPropertyContainer 
						- Subjects = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertySubject 
								- _Name = "General";
								- Metaclasses = { IRPYRawContainer 
									- size = 1;
									- value = 
									{ IPropertyMetaclass 
										- _Name = "Graphics";
										- Properties = { IRPYRawContainer 
											- size = 1;
											- value = 
											{ IProperty 
												- _Name = "FitBoxToItsTextuals";
												- _Value = "False";
												- _Type = Bool;
											}
										}
									}
								}
							}
						}
					}
					- m_type = 87;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClass";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "";
						- _name = "Display";
						- _id = GUID 3d223f02-c5c6-448c-a0aa-ba87f09292fd;
					}
					- m_pParent = GUID 33e3865d-cfb9-4022-bedd-252c952babd4;
					- m_name = { CGIText 
						- m_str = "Display";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 5;
					}
					- m_drawBehavior = 2152;
					- m_transform = 0.122757 0 0 0.193405 89.7545 21.3699 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 2 329  2 1451  1061 1451  1061 329  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- frameset = "<frameset rows=50%,50%>
<frame name=AttributeListCompartment>
<frame name=OperationListCompartment>";
					- Compartments = { IRPYRawContainer 
						- size = 2;
						- value = 
						{ CGICompartment 
							- _id = GUID c0f4504b-0604-4797-896c-089f498c679e;
							- m_name = "Attribute";
							- m_displayOption = All;
							- m_bShowInherited = 0;
							- m_bOrdered = 0;
						}
						{ CGICompartment 
							- _id = GUID 1485ea23-0416-4ec1-9c5b-52e6a23464eb;
							- m_name = "Operation";
							- m_displayOption = All;
							- m_bShowInherited = 0;
							- m_bOrdered = 0;
						}
					}
					- Attrs = { IRPYRawContainer 
						- size = 0;
					}
					- Operations = { IRPYRawContainer 
						- size = 0;
					}
				}
				
				- m_access = 'Z';
				- m_modified = 'N';
				- m_fileVersion = "";
				- m_nModifyDate = 0;
				- m_nCreateDate = 0;
				- m_creator = "";
				- m_bScaleWithZoom = 1;
				- m_arrowStyle = 'S';
				- m_pRoot = GUID 33e3865d-cfb9-4022-bedd-252c952babd4;
				- m_currentLeftTop = 0 0 ;
				- m_currentRightBottom = 0 0 ;
			}
			- _defaultSubsystem = { IHandle 
				- _m2Class = "ISubsystem";
				- _filename = "Default.sbs";
				- _subsystem = "";
				- _class = "";
				- _name = "Default";
				- _id = GUID f6003de6-2a28-4505-9967-fdb663ac3dcb;
			}
		}
	}
	- MSCS = { IRPYRawContainer 
		- size = 2;
		- value = 
		{ IMSC 
			- _id = GUID 9f52b0a7-1e50-47b6-b667-0f96b060e33e;
			- _myState = 10240;
			- _properties = { IPropertyContainer 
				- Subjects = { IRPYRawContainer 
					- size = 1;
					- value = 
					{ IPropertySubject 
						- _Name = "Format";
						- Metaclasses = { IRPYRawContainer 
							- size = 2;
							- value = 
							{ IPropertyMetaclass 
								- _Name = "EnvironmentLine";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,96,437";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Weight@Child.NameCompartment@Name";
										- _Value = "700";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "109,163,217";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "InstanceLine";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,96,437";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Weight@Child.NameCompartment@Name";
										- _Value = "700";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "109,163,217";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "0";
										- _Type = Int;
									}
								}
							}
						}
					}
				}
			}
			- _name = "sequencediagram_0";
			- _modifiedTimeWeak = 1.2.1990::0:0:0;
			- _lastModifiedTime = "11.22.2016::12:20:50";
			- _graphicChart = { CGIMscChart 
				- vLadderMargin = 20;
				- m_usingActivationBar = 0;
				- _id = GUID e7384d72-4c9a-466a-9644-b17ab3f73d73;
				- m_type = 0;
				- m_pModelObject = { IHandle 
					- _m2Class = "IMSC";
					- _id = GUID 9f52b0a7-1e50-47b6-b667-0f96b060e33e;
				}
				- m_pParent = ;
				- m_name = { CGIText 
					- m_str = "";
					- m_style = "Arial" 10 0 0 0 1 ;
					- m_color = { IColor 
						- m_fgColor = 0;
						- m_bgColor = 0;
						- m_bgFlag = 0;
					}
					- m_position = 1 0 0  ;
					- m_nIdent = 0;
					- m_bImplicitSetRectPoints = 0;
					- m_nOrientationCtrlPt = 8;
				}
				- m_drawBehavior = 0;
				- m_bIsPreferencesInitialized = 0;
				- elementList = 3;
				{ CGIBox 
					- _id = GUID 17fcc5bd-add7-4bda-91a2-b9d2d5da348e;
					- m_type = 108;
					- m_pModelObject = { IHandle 
						- _m2Class = "ICollaboration";
						- _id = GUID 1b0a3195-ae22-47e1-9165-a4dd496cce76;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_bIsPreferencesInitialized = 0;
					- m_polygon = 0 ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscColumnCR 
					- _id = GUID 3e5cdbfd-ff72-4d85-a105-a1e79e7b7539;
					- m_type = 118;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID 761647e5-c180-4afc-b7e7-d04920b9b527;
					}
					- m_pParent = GUID 17fcc5bd-add7-4bda-91a2-b9d2d5da348e;
					- m_name = { CGIText 
						- m_str = "ENV";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.00874 27 50 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 50000  96 50000  96 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscColumnCR 
					- _id = GUID ac76bb82-08e9-4dda-a0c5-0123ec2cb642;
					- m_type = 109;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID eb2fab56-3db2-4fb2-a571-a3ecbf378df9;
					}
					- m_pParent = GUID 17fcc5bd-add7-4bda-91a2-b9d2d5da348e;
					- m_name = { CGIText 
						- m_str = ":Display";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.00876015 259 50 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 49885  96 49885  96 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				
				- m_access = 'Z';
				- m_modified = 'N';
				- m_fileVersion = "";
				- m_nModifyDate = 0;
				- m_nCreateDate = 0;
				- m_creator = "";
				- m_bScaleWithZoom = 1;
				- m_arrowStyle = 'S';
				- m_pRoot = GUID 17fcc5bd-add7-4bda-91a2-b9d2d5da348e;
				- m_currentLeftTop = 0 0 ;
				- m_currentRightBottom = 0 0 ;
			}
			- _defaultSubsystem = { IHandle 
				- _m2Class = "ISubsystem";
				- _filename = "Default.sbs";
				- _subsystem = "";
				- _class = "";
				- _name = "Default";
				- _id = GUID f6003de6-2a28-4505-9967-fdb663ac3dcb;
			}
			- m_pICollaboration = { ICollaboration 
				- _id = GUID 1b0a3195-ae22-47e1-9165-a4dd496cce76;
				- _modifiedTimeWeak = 1.2.1990::0:0:0;
				- ClassifierRoles = { IRPYRawContainer 
					- size = 2;
					- value = 
					{ IClassifierRole 
						- _id = GUID 761647e5-c180-4afc-b7e7-d04920b9b527;
						- _name = "ENV";
						- _modifiedTimeWeak = 11.22.2016::12:20:35;
						- m_eRoleType = SYSTEM_BORDER;
						- m_pBase = { IHandle 
							- _m2Class = "";
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
					{ IClassifierRole 
						- _id = GUID eb2fab56-3db2-4fb2-a571-a3ecbf378df9;
						- _myState = 2048;
						- _modifiedTimeWeak = 11.22.2016::12:20:50;
						- m_eRoleType = CLASS;
						- m_pBase = { IHandle 
							- _m2Class = "IClass";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "";
							- _name = "Display";
							- _id = GUID 3d223f02-c5c6-448c-a0aa-ba87f09292fd;
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
				}
			}
		}
		{ IMSC 
			- _id = GUID 8f63723f-ff3f-4ae6-8a1e-3185310b0434;
			- _myState = 8192;
			- _properties = { IPropertyContainer 
				- Subjects = { IRPYRawContainer 
					- size = 1;
					- value = 
					{ IPropertySubject 
						- _Name = "Format";
						- Metaclasses = { IRPYRawContainer 
							- size = 7;
							- value = 
							{ IPropertyMetaclass 
								- _Name = "Condition_Mark";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,67,39";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Weight@Child.NameCompartment@Name";
										- _Value = "700";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "109,163,217";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "CreateMessage";
								- Properties = { IRPYRawContainer 
									- size = 5;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "128,128,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineStyle";
										- _Value = "1";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "0";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "DestroyMessage";
								- Properties = { IRPYRawContainer 
									- size = 5;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "128,128,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineStyle";
										- _Value = "1";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "0";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "EnvironmentLine";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,96,437";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Weight@Child.NameCompartment@Name";
										- _Value = "700";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "109,163,217";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "InstanceLine";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,96,437";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Weight@Child.NameCompartment@Name";
										- _Value = "700";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "109,163,217";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "0";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "Message";
								- Properties = { IRPYRawContainer 
									- size = 4;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "128,128,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "TimeoutMessage";
								- Properties = { IRPYRawContainer 
									- size = 4;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Tahoma";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "8";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "128,128,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
						}
					}
				}
			}
			- _name = "Animated sequencediagram_0";
			- _modifiedTimeWeak = 1.2.1990::0:0:0;
			- _lastModifiedTime = "11.22.2016::12:21:1";
			- _graphicChart = { CGIMscChart 
				- vLadderMargin = 20;
				- m_usingActivationBar = 0;
				- _id = GUID e021fe72-607b-4660-a89a-5685b7fd67fd;
				- m_type = 0;
				- m_pModelObject = { IHandle 
					- _m2Class = "IMSC";
					- _id = GUID 8f63723f-ff3f-4ae6-8a1e-3185310b0434;
				}
				- m_pParent = ;
				- m_name = { CGIText 
					- m_str = "";
					- m_style = "Arial" 10 0 0 0 1 ;
					- m_color = { IColor 
						- m_fgColor = 0;
						- m_bgColor = 0;
						- m_bgFlag = 0;
					}
					- m_position = 1 0 0  ;
					- m_nIdent = 0;
					- m_bImplicitSetRectPoints = 0;
					- m_nOrientationCtrlPt = 8;
				}
				- m_drawBehavior = 0;
				- m_bIsPreferencesInitialized = 0;
				- elementList = 53;
				{ CGIBox 
					- _id = GUID 26f9ceab-b62b-47ee-bcc4-d89a190def06;
					- m_type = 108;
					- m_pModelObject = { IHandle 
						- _m2Class = "ICollaboration";
						- _id = GUID f07c0d71-bca3-4fa4-9170-8ea10eb7927c;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_bIsPreferencesInitialized = 0;
					- m_polygon = 0 ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscColumnCR 
					- _id = GUID d6775795-6d30-488a-8d94-87b72f34dcee;
					- _properties = { IPropertyContainer 
						- Subjects = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertySubject 
								- _Name = "Format";
								- Metaclasses = { IRPYRawContainer 
									- size = 1;
									- value = 
									{ IPropertyMetaclass 
										- _Name = "EnvironmentLine";
										- Properties = { IRPYRawContainer 
											- size = 9;
											- value = 
											{ IProperty 
												- _Name = "DefaultSize";
												- _Value = "0,0,96,437";
												- _Type = String;
											}
											{ IProperty 
												- _Name = "Fill.FillColor";
												- _Value = "255,255,255";
												- _Type = Color;
											}
											{ IProperty 
												- _Name = "Fill.FillColor@Child.SDLifelineBody";
												- _Value = "109,163,217";
												- _Type = Color;
											}
											{ IProperty 
												- _Name = "Fill.Transparent_Fill@Child.SDLifelineBody";
												- _Value = "0";
												- _Type = Int;
											}
											{ IProperty 
												- _Name = "Font.Font";
												- _Value = "Tahoma";
												- _Type = String;
											}
											{ IProperty 
												- _Name = "Font.Size";
												- _Value = "8";
												- _Type = Int;
											}
											{ IProperty 
												- _Name = "Font.Weight@Child.NameCompartment@Name";
												- _Value = "700";
												- _Type = Int;
											}
											{ IProperty 
												- _Name = "Line.LineColor";
												- _Value = "109,163,217";
												- _Type = Color;
											}
											{ IProperty 
												- _Name = "Line.LineWidth";
												- _Value = "1";
												- _Type = Int;
											}
										}
									}
								}
							}
						}
					}
					- m_type = 118;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID 79b07aae-b11d-461e-afa6-bed6d3a56520;
					}
					- m_pParent = GUID 26f9ceab-b62b-47ee-bcc4-d89a190def06;
					- m_name = { CGIText 
						- m_str = "ENV";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.05042 27 50 ;
					- m_bIsPreferencesInitialized = 0;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 1 0 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscColumnCR 
					- _id = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- _properties = { IPropertyContainer 
						- Subjects = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertySubject 
								- _Name = "Format";
								- Metaclasses = { IRPYRawContainer 
									- size = 1;
									- value = 
									{ IPropertyMetaclass 
										- _Name = "InstanceLine";
										- Properties = { IRPYRawContainer 
											- size = 7;
											- value = 
											{ IProperty 
												- _Name = "DefaultSize";
												- _Value = "0,0,96,437";
												- _Type = String;
											}
											{ IProperty 
												- _Name = "Fill.FillColor";
												- _Value = "255,255,255";
												- _Type = Color;
											}
											{ IProperty 
												- _Name = "Font.Font";
												- _Value = "Tahoma";
												- _Type = String;
											}
											{ IProperty 
												- _Name = "Font.Size";
												- _Value = "8";
												- _Type = Int;
											}
											{ IProperty 
												- _Name = "Font.Weight@Child.NameCompartment@Name";
												- _Value = "700";
												- _Type = Int;
											}
											{ IProperty 
												- _Name = "Line.LineColor";
												- _Value = "109,163,217";
												- _Type = Color;
											}
											{ IProperty 
												- _Name = "Line.LineWidth";
												- _Value = "0";
												- _Type = Int;
											}
										}
									}
								}
							}
						}
					}
					- m_type = 109;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
					}
					- m_pParent = GUID 26f9ceab-b62b-47ee-bcc4-d89a190def06;
					- m_name = { CGIText 
						- m_str = ":Display";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.0505362 259 50 ;
					- m_bIsPreferencesInitialized = 0;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 1 0 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscMessage 
					- _id = GUID fd5444e4-6e5f-4e5f-bbc9-9c3b63bf80f4;
					- m_type = 112;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID e30f63a0-10e9-4ccf-9e70-a7a253ea5aa8;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "Create()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d6775795-6d30-488a-8d94-87b72f34dcee;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 1388 ;
					- m_TargetPort = 48 1385 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 4df4109c-7d47-482c-813e-7c711fac66aa;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 14018603-e96e-4f94-924a-f9f18a0eff76;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(s = Constructed)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 140  337 160  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 1781 ;
					- m_TargetPort = 48 2177 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID c4b6b627-6995-4aa8-81b4-bf0288046e96;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 7bb6b055-3626-46db-80b7-09b4c891378f;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(s = Started)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 180  337 200  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 2572 ;
					- m_TargetPort = 48 2968 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID 0eacc5f8-f488-4e91-9696-2f360a4f2fd2;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 293e2b4b-9c34-4c67-923d-647b1bdb84bb;
					}
					- m_pParent = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_name = { CGIText 
						- m_str = "Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 19.7878 0 3245.2 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID d7733307-d11c-4149-aeec-b2ccf76f5d51;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 784b6098-13f4-4cc4-a2a2-8a7497df5dda;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(200) at ROOT.Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 291  337 311  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 4769 ;
					- m_TargetPort = 48 5165 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID b2d88d05-c9f7-4be6-a516-99a7ac52edc0;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID c154b6de-e135-4e29-96ca-4072102f42c4;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 331  337 351  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 5560 ;
					- m_TargetPort = 48 5956 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID c2cd728b-173d-49e5-89e0-ce672be6b6ba;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID d3c023ef-db99-4598-803a-7bc3027b822d;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(n = 10)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 371  337 391  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 6352 ;
					- m_TargetPort = 48 6748 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID 74a0718c-d0f6-4168-a2c2-85633047f0f8;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 10116f65-9093-449e-bc42-415456edffd6;
					}
					- m_pParent = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_name = { CGIText 
						- m_str = "Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 19.7878 0 7024.66 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID 56fa8f66-2c6c-4a6a-925a-9f1b57cc0ba9;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 75d43dd8-7f83-41c2-8f8b-d815a08ee0ec;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(200) at ROOT.Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 482  337 502  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 8548 ;
					- m_TargetPort = 48 8944 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID d7802200-f286-47e7-b883-36ad23ba51b4;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 99870529-bddc-4209-832b-fd29ecdc95f4;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 522  337 542  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 9340 ;
					- m_TargetPort = 48 9736 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 4f709cc9-21d4-47d1-a72a-02f5af9eaf82;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID f6a84a1b-1c37-4355-b6b2-debdd29754b3;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(n = 9)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 562  337 582  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 10131 ;
					- m_TargetPort = 48 10527 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID aeeb3ef3-fddb-4042-a12f-2daa83e011fa;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 54d13f84-7401-48fb-8dfa-e8a05ed9e7e3;
					}
					- m_pParent = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_name = { CGIText 
						- m_str = "Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 19.7878 0 10804.1 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID 7e1db329-bd80-42a6-b1b1-05dc501500c9;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID bca224bc-ae63-4995-8c2f-f6f4dfbb3b46;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(200) at ROOT.Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 673  337 693  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 12328 ;
					- m_TargetPort = 48 12724 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID a4039d84-087d-4e0a-920f-8640642aaef1;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 28661638-9b24-4e2d-8946-3593d2b5940f;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 713  337 733  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 13119 ;
					- m_TargetPort = 48 13515 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID a5e6359c-1ca2-4345-9186-9a776738ecc2;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 359b5f1b-2b55-4414-86fb-ee5d19477ff7;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(n = 8)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 753  337 773  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 13911 ;
					- m_TargetPort = 48 14307 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID 37282c5c-ab5c-4048-bde5-9195aeefbfa7;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 13e0ceee-101a-489d-989a-ad9280146525;
					}
					- m_pParent = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_name = { CGIText 
						- m_str = "Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 19.7878 0 14583.6 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID 7cf56789-b000-4cc5-bf55-d0769b7b30d6;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 4193bf9d-1a13-4683-a2a0-f48190427f33;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(200) at ROOT.Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 864  337 884  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 16107 ;
					- m_TargetPort = 48 16503 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID bb2a50ac-19e6-4959-9a3d-4fceb3fc60ea;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID acd31052-830c-4f7b-b353-a1536916cf85;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 904  337 924  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 16899 ;
					- m_TargetPort = 48 17295 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 24d45394-1de6-4052-a4f9-70c417555544;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID efd54bb9-1d9a-45eb-bb5b-56e026438074;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(n = 7)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 944  337 964  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 17690 ;
					- m_TargetPort = 48 18086 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID dbc52642-726a-49aa-839b-bf5e4d285f25;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 34d624ef-8897-4d58-9051-36a08fcf8027;
					}
					- m_pParent = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_name = { CGIText 
						- m_str = "Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 19.7878 0 18363.1 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID 5f5913c9-25cd-4186-a40c-69cd37de85b2;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 031b0061-ae94-47a6-b7c7-4b1dee199e98;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(200) at ROOT.Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 1055  337 1075  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 19887 ;
					- m_TargetPort = 48 20282 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 57a26c68-3d78-464e-b5d9-379d082a0bc7;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID d8e4597e-ca4f-4733-96a8-c0a0c655dd71;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 1095  337 1115  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 20678 ;
					- m_TargetPort = 48 21074 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 1bb42987-0f98-4925-9815-79edac9485eb;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID afc63cef-d4a4-4692-88d9-63bb81b3e123;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(n = 6)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 1135  337 1155  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 21470 ;
					- m_TargetPort = 48 21866 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID 445e0024-f17a-4754-8593-d0f12fa8df0a;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 9a313c63-5991-4751-8002-d8a97dbf6d72;
					}
					- m_pParent = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_name = { CGIText 
						- m_str = "Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 19.7878 0 22142.5 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID f14c784b-51b6-47b8-80dd-9885bfb3548e;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 0c00e979-2101-4035-b05b-5d22abf14c65;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(200) at ROOT.Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 1246  337 1266  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 23666 ;
					- m_TargetPort = 48 24062 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 4e8c3eac-ffea-4b2b-9756-9af2a5ed48e1;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 9bd41d82-e91c-4e80-82fd-4283dbd2bd85;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 1286  337 1306  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 24458 ;
					- m_TargetPort = 48 24853 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 25aca455-f96d-4ab4-819d-4140e67f9deb;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID ae4f551d-092c-49b1-a7fb-bed915b12693;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(n = 5)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 1326  337 1346  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 25249 ;
					- m_TargetPort = 48 25645 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID 1cf9594d-ff95-48dd-9535-1b0bc7a0c9bc;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 4cd97470-e9c1-4221-b23a-6a787d009a78;
					}
					- m_pParent = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_name = { CGIText 
						- m_str = "Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 19.7878 0 25922 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID 706767e2-1e8b-415f-9817-bccfd4b7264d;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 7677e5ab-4bd3-4df4-9f37-cb1463ae26b8;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(200) at ROOT.Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 1437  337 1457  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 27446 ;
					- m_TargetPort = 48 27841 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID b83aef70-5ca9-4a55-bebb-d70acc3d45b7;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID f9ce10c4-ff27-4e19-ad76-6f3d82cfe2c7;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 1477  337 1497  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 28237 ;
					- m_TargetPort = 48 28633 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID a7075c21-2a55-4c47-be6c-78b4e82ea915;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID d4b943cc-7bf9-4010-8811-b15eedb07abf;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(n = 4)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 1517  337 1537  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 29029 ;
					- m_TargetPort = 48 29424 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID 4ca7fae2-c6a4-41c1-b42f-bdf88e24aa1b;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 024ab18a-e58d-48f1-9600-47599728c6eb;
					}
					- m_pParent = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_name = { CGIText 
						- m_str = "Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 19.7878 0 29701.5 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID 8e790b8b-8177-4af2-bdb0-2e8b58a36f06;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 78bd27dc-4677-4698-b1a5-d8f871b75646;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(200) at ROOT.Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 1628  337 1648  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 31225 ;
					- m_TargetPort = 48 31621 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 99c9f104-fcaf-48b0-a442-2a006da1e4a3;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 7969f3f0-f161-48e5-9a02-1645475b5741;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 1668  337 1688  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 32017 ;
					- m_TargetPort = 48 32412 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 113c2476-d10f-45ae-88e3-62de220d0be9;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID ea4c9a7c-9604-4dfd-b868-e99cbc262bef;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(n = 3)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 1708  337 1728  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 32808 ;
					- m_TargetPort = 48 33204 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID 7fd0dd0a-0dc2-4fcb-bb32-68977b50e0b3;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 4c8b0627-b70a-4aea-8c27-bc5d26170e80;
					}
					- m_pParent = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_name = { CGIText 
						- m_str = "Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 19.7878 0 33480.9 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID d634a04b-5452-4919-962b-6698e57658da;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 16fe2cf8-5225-4ad8-8fa2-c60e74454083;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(200) at ROOT.Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 1819  337 1839  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 35005 ;
					- m_TargetPort = 48 35400 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID ce4e6cc2-a791-44d1-a9e5-3bf2201a9c82;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 244f4420-878e-48a5-b873-74c519950a83;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 1859  337 1879  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 35796 ;
					- m_TargetPort = 48 36192 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 2981bf5d-23c9-4127-b9cc-a9dc05969981;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 92848cb8-109c-4a1d-9581-652cb5b3b3c9;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(n = 2)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 1899  337 1919  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 36588 ;
					- m_TargetPort = 48 36983 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID e63dbb25-3bc0-4a26-926a-d448c44565a8;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 034f6ff8-cc24-4aee-a6e3-da6669e9f269;
					}
					- m_pParent = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_name = { CGIText 
						- m_str = "Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 19.7878 0 37260.4 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID c89f5f25-1699-4f85-9ef6-ddc5baea9def;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 4bfc4ed2-f307-4902-8c9e-a0e6a57bc478;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(200) at ROOT.Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 2010  337 2030  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 38784 ;
					- m_TargetPort = 48 39180 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID ae6345dc-585d-4478-b861-aec43ab11f41;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID ea017365-f0c0-4710-8ef7-fee0c06102e9;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 2050  337 2070  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 39576 ;
					- m_TargetPort = 48 39971 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 3cffce1f-da38-497d-9740-035b9f775b91;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID db42c3d1-a299-4e79-b6a1-2ba8671fe355;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(n = 1)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 2090  337 2110  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 40367 ;
					- m_TargetPort = 48 40763 ;
					- m_bLeft = 0;
				}
				{ CGIMscConditionMark 
					- _id = GUID a110756c-621e-4df6-9b5c-551255e31a88;
					- m_type = 114;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 0f59d633-de71-4086-b0de-10e0c8e26514;
					}
					- m_pParent = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_name = { CGIText 
						- m_str = "Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.14286 0 0 19.7878 0 41039.9 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 51  84 51  84 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Compartments = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIMscMessage 
					- _id = GUID 0d69f63a-94d7-4345-a8b6-57d791eb5a0b;
					- m_type = 115;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID c483ef6c-ed77-4341-bda4-78f41db0c450;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "tm(200) at ROOT.Active";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 2201  337 2221  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 42564 ;
					- m_TargetPort = 48 42959 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 2f8ebf79-31dd-47aa-926d-9c59f96e2453;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 2e17e141-edc5-4689-807d-f4abac42ac53;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "isDone()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 2241  337 2261  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 43355 ;
					- m_TargetPort = 48 43751 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID b980e048-91d5-4d42-8ab4-1832204c0d13;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID f589b3e5-a8cf-42fd-a279-349356e5c0b1;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(n = 0)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 2281  337 2301  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 44147 ;
					- m_TargetPort = 48 44542 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID fecf9154-cab8-417d-bce2-ac3f727b6262;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 68b9799e-9304-4fc5-ab32-71b26ae11f81;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(s = Done)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 2321  337 2341  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 44938 ;
					- m_TargetPort = 48 45334 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID b85ed9eb-3622-4c43-8c6e-85a06bf87ca6;
					- m_type = 113;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 4f98ff75-46da-48dd-9eca-4655878e38be;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "Destroy()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d6775795-6d30-488a-8d94-87b72f34dcee;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 45835 ;
					- m_TargetPort = 48 45730 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 26c6a863-7ac3-462b-9e68-327dbea67278;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 2e5ff5c9-9f58-48f4-bb05-3b259aa03ef4;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "print(s = Destroyed)";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d522b135-c271-4d6f-b86e-4a4cc7eec66b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 337 2381  337 2401  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 46125 ;
					- m_TargetPort = 48 46521 ;
					- m_bLeft = 0;
				}
				
				- m_access = 'Z';
				- m_modified = 'N';
				- m_fileVersion = "";
				- m_nModifyDate = 0;
				- m_nCreateDate = 0;
				- m_creator = "";
				- m_bScaleWithZoom = 1;
				- m_arrowStyle = 'S';
				- m_pRoot = GUID 26f9ceab-b62b-47ee-bcc4-d89a190def06;
				- m_currentLeftTop = 0 0 ;
				- m_currentRightBottom = 0 0 ;
			}
			- _defaultSubsystem = { IHandle 
				- _m2Class = "ISubsystem";
				- _filename = "Default.sbs";
				- _subsystem = "";
				- _class = "";
				- _name = "Default";
				- _id = GUID f6003de6-2a28-4505-9967-fdb663ac3dcb;
			}
			- m_pICollaboration = { ICollaboration 
				- _id = GUID f07c0d71-bca3-4fa4-9170-8ea10eb7927c;
				- _modifiedTimeWeak = 1.2.1990::0:0:0;
				- ClassifierRoles = { IRPYRawContainer 
					- size = 2;
					- value = 
					{ IClassifierRole 
						- _id = GUID 79b07aae-b11d-461e-afa6-bed6d3a56520;
						- _name = "ENV";
						- _modifiedTimeWeak = 11.22.2016::12:20:35;
						- m_eRoleType = SYSTEM_BORDER;
						- m_pBase = { IHandle 
							- _m2Class = "";
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
					{ IClassifierRole 
						- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						- _myState = 2048;
						- _modifiedTimeWeak = 11.22.2016::12:20:50;
						- m_eRoleType = CLASS;
						- m_pBase = { IHandle 
							- _m2Class = "IClass";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "";
							- _name = "Display";
							- _id = GUID 3d223f02-c5c6-448c-a0aa-ba87f09292fd;
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
				}
				- Messages = { IRPYRawContainer 
					- size = 50;
					- value = 
					{ IMessage 
						- _id = GUID e30f63a0-10e9-4ccf-9e70-a7a253ea5aa8;
						- _name = "Create";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "1.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 79b07aae-b11d-461e-afa6-bed6d3a56520;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CREATE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 14018603-e96e-4f94-924a-f9f18a0eff76;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "2.";
						- m_szActualArgs = "s = Constructed";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 98593c22-b924-4473-bc99-de3df4ce9dd3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 7bb6b055-3626-46db-80b7-09b4c891378f;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "3.";
						- m_szActualArgs = "s = Started";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 98593c22-b924-4473-bc99-de3df4ce9dd3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 293e2b4b-9c34-4c67-923d-647b1bdb84bb;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 784b6098-13f4-4cc4-a2a2-8a7497df5dda;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "4.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID c154b6de-e135-4e29-96ca-4072102f42c4;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "5.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 629f7a7b-45d2-4777-961a-c630c7e59c16;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID d3c023ef-db99-4598-803a-7bc3027b822d;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "6.";
						- m_szActualArgs = "n = 10";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 98593c22-b924-4473-bc99-de3df4ce9dd3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 10116f65-9093-449e-bc42-415456edffd6;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 75d43dd8-7f83-41c2-8f8b-d815a08ee0ec;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "7.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 99870529-bddc-4209-832b-fd29ecdc95f4;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "8.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 629f7a7b-45d2-4777-961a-c630c7e59c16;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID f6a84a1b-1c37-4355-b6b2-debdd29754b3;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "9.";
						- m_szActualArgs = "n = 9";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 98593c22-b924-4473-bc99-de3df4ce9dd3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 54d13f84-7401-48fb-8dfa-e8a05ed9e7e3;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID bca224bc-ae63-4995-8c2f-f6f4dfbb3b46;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "10.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 28661638-9b24-4e2d-8946-3593d2b5940f;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "11.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 629f7a7b-45d2-4777-961a-c630c7e59c16;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 359b5f1b-2b55-4414-86fb-ee5d19477ff7;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "12.";
						- m_szActualArgs = "n = 8";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 98593c22-b924-4473-bc99-de3df4ce9dd3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 13e0ceee-101a-489d-989a-ad9280146525;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 4193bf9d-1a13-4683-a2a0-f48190427f33;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "13.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID acd31052-830c-4f7b-b353-a1536916cf85;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "14.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 629f7a7b-45d2-4777-961a-c630c7e59c16;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID efd54bb9-1d9a-45eb-bb5b-56e026438074;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "15.";
						- m_szActualArgs = "n = 7";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 98593c22-b924-4473-bc99-de3df4ce9dd3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 34d624ef-8897-4d58-9051-36a08fcf8027;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 031b0061-ae94-47a6-b7c7-4b1dee199e98;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "16.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID d8e4597e-ca4f-4733-96a8-c0a0c655dd71;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "17.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 629f7a7b-45d2-4777-961a-c630c7e59c16;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID afc63cef-d4a4-4692-88d9-63bb81b3e123;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "18.";
						- m_szActualArgs = "n = 6";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 98593c22-b924-4473-bc99-de3df4ce9dd3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 9a313c63-5991-4751-8002-d8a97dbf6d72;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 0c00e979-2101-4035-b05b-5d22abf14c65;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "19.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 9bd41d82-e91c-4e80-82fd-4283dbd2bd85;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "20.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 629f7a7b-45d2-4777-961a-c630c7e59c16;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID ae4f551d-092c-49b1-a7fb-bed915b12693;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "21.";
						- m_szActualArgs = "n = 5";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 98593c22-b924-4473-bc99-de3df4ce9dd3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 4cd97470-e9c1-4221-b23a-6a787d009a78;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 7677e5ab-4bd3-4df4-9f37-cb1463ae26b8;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "22.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID f9ce10c4-ff27-4e19-ad76-6f3d82cfe2c7;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "23.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 629f7a7b-45d2-4777-961a-c630c7e59c16;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID d4b943cc-7bf9-4010-8811-b15eedb07abf;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "24.";
						- m_szActualArgs = "n = 4";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 98593c22-b924-4473-bc99-de3df4ce9dd3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 024ab18a-e58d-48f1-9600-47599728c6eb;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 78bd27dc-4677-4698-b1a5-d8f871b75646;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "25.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 7969f3f0-f161-48e5-9a02-1645475b5741;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "26.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 629f7a7b-45d2-4777-961a-c630c7e59c16;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID ea4c9a7c-9604-4dfd-b868-e99cbc262bef;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "27.";
						- m_szActualArgs = "n = 3";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 98593c22-b924-4473-bc99-de3df4ce9dd3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 4c8b0627-b70a-4aea-8c27-bc5d26170e80;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 16fe2cf8-5225-4ad8-8fa2-c60e74454083;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "28.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 244f4420-878e-48a5-b873-74c519950a83;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "29.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 629f7a7b-45d2-4777-961a-c630c7e59c16;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 92848cb8-109c-4a1d-9581-652cb5b3b3c9;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "30.";
						- m_szActualArgs = "n = 2";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 98593c22-b924-4473-bc99-de3df4ce9dd3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 034f6ff8-cc24-4aee-a6e3-da6669e9f269;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 4bfc4ed2-f307-4902-8c9e-a0e6a57bc478;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "31.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID ea017365-f0c0-4710-8ef7-fee0c06102e9;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "32.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 629f7a7b-45d2-4777-961a-c630c7e59c16;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID db42c3d1-a299-4e79-b6a1-2ba8671fe355;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "33.";
						- m_szActualArgs = "n = 1";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 98593c22-b924-4473-bc99-de3df4ce9dd3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 0f59d633-de71-4086-b0de-10e0c8e26514;
						- _properties = { IPropertyContainer 
							- Subjects = { IRPYRawContainer 
								- size = 1;
								- value = 
								{ IPropertySubject 
									- _Name = "SequenceDiagram";
									- Metaclasses = { IRPYRawContainer 
										- size = 1;
										- value = 
										{ IPropertyMetaclass 
											- _Name = "Condition_Mark";
											- Properties = { IRPYRawContainer 
												- size = 1;
												- value = 
												{ IProperty 
													- _Name = "IsStateCondition";
													- _Value = "True";
													- _Type = Bool;
												}
											}
										}
									}
								}
							}
						}
						- _name = "connector";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = CONDITION;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID c483ef6c-ed77-4341-bda4-78f41db0c450;
						- _myState = 8192;
						- _name = "tm";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "34.";
						- m_szActualArgs = "200";
						- m_szReturnVal = "";
						- m_freeText = " at ROOT.Active";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = TIMEOUT;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 2e17e141-edc5-4689-807d-f4abac42ac53;
						- _name = "isDone";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "35.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "isDone()";
							- _id = GUID 629f7a7b-45d2-4777-961a-c630c7e59c16;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID f589b3e5-a8cf-42fd-a279-349356e5c0b1;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "36.";
						- m_szActualArgs = "n = 0";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 98593c22-b924-4473-bc99-de3df4ce9dd3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 68b9799e-9304-4fc5-ab32-71b26ae11f81;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "37.";
						- m_szActualArgs = "s = Done";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 98593c22-b924-4473-bc99-de3df4ce9dd3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 4f98ff75-46da-48dd-9eca-4655878e38be;
						- _name = "Destroy";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "38.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 79b07aae-b11d-461e-afa6-bed6d3a56520;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DESTROY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 2e5ff5c9-9f58-48f4-bb05-3b259aa03ef4;
						- _myState = 8192;
						- _name = "print";
						- _modifiedTimeWeak = 1.2.1990::0:0:0;
						- m_szSequence = "39.";
						- m_szActualArgs = "s = Destroyed";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID f20c8fe7-e624-4e5d-bc72-7edfb78c5b8b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Display";
							- _name = "print(int)";
							- _id = GUID 98593c22-b924-4473-bc99-de3df4ce9dd3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
				}
			}
		}
	}
	- Components = { IRPYRawContainer 
		- size = 1;
		- value = 
		{ IComponent 
			- fileName = "Test";
			- _id = GUID d4aad5ad-d232-4758-8942-f0e81b6cf7be;
		}
	}
}

