# Exercise 3.11

Modify class GradeBook (Figs. 3.11–3.12) as follows:

  - **a)** *Include a second string data member that represents the course instructor’s name.*  
    Add the following to the `private` section of `GradeBook.h`:
    ```c++
    std::string courseInstructor; // course instructor for this GradeBook
    ```

  - **b)** *Provide a set function to change the instructor’s name and a get function to retrieve it.*  
    This is identical to the set and get functions for the course name with `instructor` instead of `name`,
    in `GradeBook.h` add:
    ```c++
    void setCourseInstructor(std::string);        // sets the course instructor
    std::string getCourseInstructor() const;      // gets the course instructor
    ```
    
    And in `GradeBook.cpp`, add:
    ```c++
    // function to set the course instructor
    void GradeBook::setCourseInstructor(string instructor) {
        courseInstructor = instructor; // store the course instructor in the object
    } // end function setCourseInstructor
    
    // function to get the course instructor
    string GradeBook::getCourseInstructor() const {
        return courseInstructor; // return object's courseInstructor
    } // end function getCourseInstructor
    ```
    
  - **c)** *Modify the constructor to specify course name and instructor name parameters.*  
    This requires changes in `GradeBook.h`:
    ```c++
    explicit GradeBook(std::string, std::string); // constructor initialize courseName, courseInstructor
    ```
    
    And in `GradeBook.cpp`:
    ```c++
    // constructor initializes courseName and courseInstructor with strings supplied as argument
    GradeBook::GradeBook(string name, string instructor)
            : courseName(name),            // member initializer to initialize courseName
              courseInstructor(instructor) // member initializer to initialize courseInstructor
    ```
    
  - **d)** *Modify function displayMessage to output the welcome message and course name,
    then the string "This course is presented by: " followed by the instructor’s name.*  
    This only requires modifications in `GradeBook.cpp`:
    ```c++
    // call getCourseName to get the courseName and getCourseInstructor to get the courseInstructor
    cout << "Welcome to the grade book for\n" << getCourseName()
         << "!\n"
         << "This course is presented by: " << getCourseInstructor()
         << endl;
    ```

The example file (`exercise.cpp`) is modified to reflect above changes.
It constructs two grade books and displays their messages:

```
gradeBook1 created for course: CS101 Introduction to C++ Programming
gradeBook2 created for course: CS102 Data Structures in C++
Welcome to the grade book for
CS101 Introduction to C++ Programming!
This course is presented by: Jan
Welcome to the grade book for
CS102 Data Structures in C++!
This course is presented by: Wolfgang
```
