// Fig. 3.12: GradeBook.cpp
// GradeBook member-function definitions. This file contains
// implementations of the member functions prototyped in GradeBook.h.
#include <iostream>
#include "GradeBook.h" // include definition of class GradeBook

using namespace std;

// constructor initializes courseName and courseInstructor with strings supplied as argument
GradeBook::GradeBook(string name, string instructor)
        : courseName(name),            // member initializer to initialize courseName
          courseInstructor(instructor) // member initializer to initialize courseInstructor
{
    // empty body
} // end GradeBook constructor

// function to set the course name
void GradeBook::setCourseName(string name) {
    courseName = name; // store the course name in the object
} // end function setCourseName

// function to set the course instructor
void GradeBook::setCourseInstructor(string instructor) {
    courseInstructor = instructor; // store the course instructor in the object
} // end function setCourseInstructor

// function to get the course name
string GradeBook::getCourseName() const {
    return courseName; // return object's courseName
} // end function getCourseName

// function to get the course instructor
string GradeBook::getCourseInstructor() const {
    return courseInstructor; // return object's courseInstructor
} // end function getCourseInstructor

// display a welcome message to the GradeBook user
void GradeBook::displayMessage() const {
    // call getCourseName to get the courseName and getCourseInstructor to get the courseInstructor
    cout << "Welcome to the grade book for\n" << getCourseName()
         << "!\n"
         << "This course is presented by: " << getCourseInstructor()
         << endl;
} // end function displayMessage


/**************************************************************************
 * (C) Copyright 1992-2012 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 **************************************************************************/   
