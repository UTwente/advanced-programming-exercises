// Fig. 5.11: fig05_11.cpp
// Creating a GradeBook object and calling its member functions.
#include "GradeBook.h" // include definition of class GradeBook

int main() {
    // create GradeBook object
    GradeBook myGradeBook("CS101 C++ Programming");

    Student* jan      = new Student("Jan");
    Student* wolfgang = new Student("Wolfgang");
    Student* michael  = new Student("Michael");

    // This is C++11 and does not work in VS2012
    /*
    jan->addGrades({6.5,8.5,6});
    wolfgang->addGrades({7,8});
    michael->addGrades({9,4.5,7.5});
    */
    jan->addGrade(6.5);
    jan->addGrade(8.5);
    jan->addGrade(6);
    wolfgang->addGrade(7);
    wolfgang->addGrade(8);
    michael->addGrade(9);
    michael->addGrade(4.5);
    michael->addGrade(7.5);

    myGradeBook.storeStudent(jan);
    myGradeBook.storeStudent(wolfgang);
    myGradeBook.storeStudent(michael);

    myGradeBook.sortStudents();
    myGradeBook.displayGradeReport();
} // end main


/**************************************************************************
 * (C) Copyright 1992-2014 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 **************************************************************************/
