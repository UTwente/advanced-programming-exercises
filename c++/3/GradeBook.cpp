// Fig. 5.10: GradeBook.cpp
// Member-function definitions for class GradeBook that
// uses a switch statement to count A, B, C, D and F grades.
#include <iostream>
#include "GradeBook.h" // include definition of class GradeBook

using namespace std;

// constructor initializes courseName with string supplied as argument;
// initializes counter data members to 0
GradeBook::GradeBook(string name)
{
    students.clear();
    setCourseName(name);
} // end GradeBook constructor

// function to set the course name; limits name to 25 or fewer characters
void GradeBook::setCourseName(string name) {
    if (name.size() <= 25) // if name has 25 or fewer characters
        courseName = name; // store the course name in the object
    else // if name is longer than 25 characters
    { // set courseName to first 25 characters of parameter name
        courseName = name.substr(0, 25); // select first 25 characters
        cerr << "Name \"" << name << "\" exceeds maximum length (25).\n"
             << "Limiting courseName to first 25 characters.\n" << endl;
    } // end if...else
} // end function setCourseName

// function to retrieve the course name
string GradeBook::getCourseName() const {
    return courseName;
} // end function getCourseName

// display a welcome message to the GradeBook user
void GradeBook::displayMessage() const {
    // this statement calls getCourseName to get the
    // name of the course this GradeBook represents
    cout << "Welcome to the grade book for\n" << getCourseName() << "!\n"
         << endl;
} // end function displayMessage

// input arbitrary number of grades from user; update grade counter
void GradeBook::inputGrades() {
    // not yet implemented
} // end function inputGrades

// display a report based on the grades entered by user
void GradeBook::displayGradeReport() const {
    unsigned int maxGrades = 0;
    for(Student* student : students)
        if(student->getGrades().size() > maxGrades)
            maxGrades = student->getGrades().size();

    for(Student* student : students)
        student->displayGradeReport(maxGrades);
} // end function displayGradeReport

// Store a student
void GradeBook::storeStudent(Student* student) {
    students.push_back(student);
}

// Sort the student list
void GradeBook::sortStudents() {
    students.sort([](const Student* s1, const Student* s2) {
        return s1->calcGradeAverage() < s2->calcGradeAverage();
    });
}

/**************************************************************************
 * (C) Copyright 1992-2014 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 **************************************************************************/
