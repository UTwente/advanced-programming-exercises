# Extension using pointers and strings.
*Add a class Student besides the class GradeBook, which contains the student‘s name, grades and average.*  
The `GradeBook` class is refactored to the student class.
The student class has new attributes `name` and `grades`,
and new methods `addGrade()`, `addGrades()`, `getGrades()`, `clearGrades()` and `calcGradeAverage()`.

`Student.h` shows these changes:
\clisting{7}{22}{3/Student.h}

Because of the use of `std::list` all new implementations are straight forward,
using the interfaces that the standard library offers:

\clisting{35}{53}{3/Student.cpp}
\clisting{83}{96}{3/Student.cpp}

*Create a gradeArray, consisting of pointers to objects student.*
The variable `std::list<Student*> students` is declared to store pointers to `Student` objects.
The method `storeStudent()` is declared to add a student to this list.

\clisting{18}{18}{3/GradeBook.h}
\clisting{21}{21}{3/GradeBook.h}
\clisting{58}{61}{3/GradeBook.cpp}

*Create a memberfunction which prints the grades as a matrix with on each row the
results of one student, that is like this:*

|          |     |     |     |              |
|----------|-----|-----|-----|--------------|
| Jan      | 6.5 | 8.5 | 6.0 | Average: 7.0 | 
| Wolfgang | 7.0 | 8.0 |     | Average: 7.5 |
| Michael  | 9.0 | 4.5 | 7.5 | Average: 7.0 |

In `Student` the method `displayGradeReport()` shows the grade report for a student:

\clisting{55}{81}{3/Student.cpp}

In `GradeBook` the method `displayGradeReport()` calculates the highest amount of grades and
lets the students show their grade report:

\clisting{47}{56}{3/GradeBook.cpp}

The output is:
```
 | Jan                    | 6.5 | 8.5 | 6.0 | Average: 7.0 | 
 | Wolfgang               | 7.0 | 8.0 |     | Average: 7.5 | 
 | Michael                | 9.0 | 4.5 | 7.5 | Average: 7.0 | 
```

*Create a memberfunction in the GradeBook class, which sorts the pointers in the
gradeArray such that the student‘s average is ascending.*

The `sortStudents()` method uses `std::list`'s sort method for sorting the list of students.
    
\clisting{63}{68}{3/GradeBook.cpp}

The output now is:
```
 | Jan                    | 6.5 | 8.5 | 6.0 | Average: 7.0 | 
 | Michael                | 9.0 | 4.5 | 7.5 | Average: 7.0 | 
 | Wolfgang               | 7.0 | 8.0 |     | Average: 7.5 | 
```