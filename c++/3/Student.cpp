// Fig. 5.10: Student.cpp
// Member-function definitions for class Student that
// uses a switch statement to count A, B, C, D and F grades.
#include <iostream>
#include <iomanip>
#include <numeric>
#include "Student.h" // include definition of class Student

using namespace std;

// constructor initializes name with string supplied as argument;
Student::Student(string name)
{
    clearGrades();
    setName(name);
} // end Student constructor

// function to set the student name; limits name to 25 or fewer characters
void Student::setName(string studentName) {
    if (studentName.size() <= 25) // if name has 25 or fewer characters
        name = studentName; // store the course name in the object
    else // if name is longer than 25 characters
    { // set name to first 25 characters of parameter name
        name = studentName.substr(0, 25); // select first 25 characters
        cerr << "Name \"" << studentName << "\" exceeds maximum length (25).\n"
             << "Limiting name to first 25 characters.\n" << endl;
    } // end if...else
} // end function setName

// function to retrieve the course name
string Student::getName() const {
    return name;
} // end function getName

// retrieve grades
std::list<float> Student::getGrades() const {
    return grades;
};

// Add a grade for this student
void Student::addGrade(float grade) {
    grades.push_back(grade);
}

// Add a list of grades to this student
void Student::addGrades(std::list<float> newGrades) {
    grades.merge(newGrades);
}

// Clear grades
void Student::clearGrades() {
    grades.clear();
}

// display a report based on the grades entered by user
void Student::displayGradeReport(int maxGrades) const {
    string sep = " | ";

    // Set cout to fixed point with 1 decimal
    cout << fixed;
    cout.precision(1);

    // Output name
    cout << sep << name << setw(25 - name.length()) << sep;

    // Output grades
    int gradeCount = 0;
    for(float g : grades) {
        cout << setw(3) << g << sep;
        if(++gradeCount > maxGrades)
            break;
    }

    // Output whitespace when needed
    if(gradeCount < maxGrades)
        for(int i = gradeCount; i < maxGrades; i++)
            cout << "   " << sep;

    // output average grade
    cout << "Average: " << setw(3) << calcGradeAverage() << sep << endl;
} // end function displayGradeReport

// calculate grade average
float Student::calcGradeAverage() const {
    // Check for division by zero and exit on error
    if(grades.empty()) {
        cerr << "ERROR: no grades have been registered for " << getName() << endl;
        exit(1);
    }

    // Count grades
    float points = std::accumulate(grades.begin(), grades.end(), 0.0);

    // Return grade average
    return points / grades.size();
}

/**************************************************************************
 * (C) Copyright 1992-2014 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 **************************************************************************/
