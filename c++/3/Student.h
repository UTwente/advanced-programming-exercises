// Fig. 5.9: Student.h
// Student class definition that counts letter grades.
// Member functions are defined in Student.cpp
#include <string> // program uses C++ standard string class
#include <list>   // program uses C++ standard list class

// Student class definition
class Student {
public:
    explicit Student(std::string);      // initialize course name
    void setName(std::string);          // set the student's name
    std::string getName() const;        // retrieve the student's name
    std::list<float> getGrades() const; // retrieve the list of grades
    void addGrade(float);                 // add a grade for the student
    void addGrades(std::list<float>);   // add a list of grades to the student
    void clearGrades();                 // delete all grades for the student
    void displayGradeReport(int) const; // display report for student
    float calcGradeAverage() const;     // calculate grade average
private:
    std::string name;           // name for this student
    std::list<float> grades;    // list of grades for this student
}; // end class Student  

/**************************************************************************
 * (C) Copyright 1992-2014 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 **************************************************************************/
