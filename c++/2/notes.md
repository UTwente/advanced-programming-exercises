# Exercise 5.15
*Modify the GradeBook program of Figs. 5.9–5.11 to calculate the grade-point average.
A grade of A is worth 4 points, B is worth 3 points, and so on.*

`calcGPA()` is added to calculate the GPA.
This method works by counting the amount of grades and the total number of points.
If there are grades registered, the GPA is calculated.
When there are no grades, the program exits with an error.

\clisting{111}{131}{2/GradeBook.cpp}

The method is called in `displayGradeReport()`:

\clisting{107}{108}{2/GradeBook.cpp}

A definition is also placed in `GradeBook.h`:

\clisting{15}{15}{2/GradeBook.h}

The method works as expected. The output is:

```
Welcome to the grade book for
CS101 C++ Programming!

Enter the letter grades.
Enter the EOF character to end input.
A A A A 
B
C C C
D D
F

Number of students who received each letter grade:
A: 4
B: 1
C: 3
D: 2
F: 1

GPA: 2.45455
```